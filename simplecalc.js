const http=require("http")
const express=require('express')
const app=express()
const server=http.createServer((req,res)=>{
//Add numbers API
app.get('/add', (req, res) => {
    let numbers = req.query.numbers.split(','); //convert string to array
    let sum = 0;
  
    for (let i = 0; i < numbers.length; i++) {
      sum += parseInt(numbers[i]);
    }
  
    res.send({
      result: sum
    });
  });
  
  //Multiply numbers API
  app.get('/multiply', (req, res) => {
    let numbers = req.query.numbers.split(',');
    let product = 1;
  
    for (let i = 0; i < numbers.length; i++) {
      product *= parseInt(numbers[i]);
    }
  
    res.send({
      result: product
    });
  });
  
})
server.listen(3000,()=>{
    console.log('listening to port 3000')
})